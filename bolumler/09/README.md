## 09: .gitignore

Git repository'sine eklemek istemediğimiz dosyaları `.gitignore` dosyasında
belirtiriz. 

```
$ ls -a  # index.html, css/ ve .git/ dışında birşey var mı?
         # varsa .gitignore'a eklenecek.
```

Proje dizininde `.gitignore` dosyası oluşturup aşağıdakileri
ekleyelim.

```
*~
.DS_Store
```

Sonra, `notlar.txt` dosyası oluşturup içine aşağıdaki cümleyi yazalım:

```
Ziyaretçi defteri eklemeyi unutma!
```

Tekrar duruma bakalım:

```
$ git status # Untracked files: .gitignore ve notlar.txt
```

Sonra, `.gitignore` dosyasını aşağıdaki gibi güncelleyelim:

```
*~
.DS_Store
/notlar.txt
```

Komutlar:

```
$ git status  # Untracked files: .gitignore
$ git add .gitignore
$ git commit -m ".gitignore eklendi"
```

`notlar.txt` dosyasını tekrar açıp yeni bir not ekleyelim ve duruma bakalım:

```
$ git status # nothing to commit, working tree clean
```

Bu sefer, `sifreler.txt` dosyası oluşturalım ve içine `12345` yazalım.

```
$ git status # Untracked files: sifreler.txt
```

Sonra, `.gitignore` dosyasını aşağıdaki gibi güncelleyelim:

```
*~
.DS_Store
/notlar.txt
/sifreler.txt
```

Komutlar:

```
$ git status  # Changes not staged for commit: .gitignore

$ git add .gitignore
$ git commit -m ".gitignore güncellendi"
$ git status  # nothing to commit, working tree clean
```

Bu aşamadan sonra `notlar.txt` ve `sifreler.txt` dosyalarında yapılan
değişiklikler git reposuna kaydedilmeyecek.

Örnek `.gitignore` dosyaları: https://github.com/github/gitignore

---

**NOT:** `git help gitignore`
