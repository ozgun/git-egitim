## 01: "git" Kurulumu


### Linux

```
$ sudo apt-get install git  # Debian, Ubuntu, vs.
$ sudo dnf install git-all  # RHEL, CentOS, vs.
```

### Windows

* https://git-scm.com/download/win
* https://gitforwindows.org

### Mac OS

* XCode veya XCode'un Commmand Line Tools ile otomatik kuruluyor.
* https://sourceforge.net/projects/git-osx-installer/files/
* MacPorts: `sudo port install git +bash_completion+credential_osxkeychain+doc`
* Homebrew: `brew install git` ve `brew upgrade git`

### Kurulumun Kontrol Edilmesi

```
$ git --version
```

---

**NOT:** [Pro Git e-Kitap: Git Kurulumu](https://git-scm.com/book/tr/v2/Başlangıç-Git-Kurulumu), [Atlassian: Install Git](https://www.atlassian.com/git/tutorials/install-git)

