## 12: Dosyaların Silinmesi

Boş bir `deneme.txt` dosyası oluşturalım ve onu repository'ye ekleyelim.

```
$ touch deneme.txt  # Boş bir dosya oluşturur
$ git status        # Untracked files: deneme.txt
$ git add deneme.txt
$ git commit -m "deneme.txt eklendi"
$ git status  # nothing to commit, working tree clean
```

Sonra, `deneme.txt` dosyasını diskten ve repository'den silelim:

```
$ git rm deneme.txt # Şöyle bir çıktı görülmeli: rm 'deneme.txt'
$ git status        # Changes to be committed -> deleted: deneme.txt

$ git commit -m "deneme.txt silindi"
$ git status  # nothing to commit, working tree clean
```

---

**NOT**: `git help rm`, [Pro Git e-Kitap: Recording Changes to the Repository](https://git-scm.com/book/tr/v2/Git-Temelleri-Recording-Changes-to-the-Repository)
