## 13: Commit History

```
$ git log       # Ileri: Enter. Sonraki safya: Space Bar. Çıkış: q. 
$ git log -2    # Son iki commit'in bilgilerini listele
$ git log -p    # Tüm commitler'i diffleri ile birlikte listele
$ git log -p -3 # Son 3 commit'i diffleri ile birlikte listele
$ git log --oneline    # Tüm commitlerin kısa/özet gösterimi
$ git log --oneline -1 # En son commit'in kısa/özet gösterimi
$ git log --pretty=format:"%h - %an, %ar : %s" # Özel format
$ git log --since=2.weeks # Son 2 haftada yapılan commitleri listler
```

Devamı az sonra :)

---

**NOT:** `git help log`, [Pro Git e-Kitap: Viewing the Commit History](https://git-scm.com/book/tr/v2/Git-Temelleri-Viewing-the-Commit-History)
