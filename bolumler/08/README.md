## 08: diff && commit

`index.html` dosyasını aşağıdaki gibi güncelleyelim:

```
<!DOCTYPE html>
<html>
  <head>
    <link rel="stylesheet" href="css/main.css" type="text/css">
  </head>
  <body>
    <h1>Trakya Yazılım Geliştiricileri Platformu</h1>
  </body>
</html>
```

Komutlar:

```
$ git status           # modified: index.html -> Changes not staged for commit

$ git diff             # Değişiklikleri görelim
$ git diff index.html  # Sadece bir dosyada yapılan değişiklikleri gösterir
$ git add index.html   # Staging Area'ya alalım
$ git status           # Changes to be committed: index.html

$ git commit -m "css dosyasına referans"  # 1 file changed, 3 insertions(+)
$ git status                              # nothing to commit, working tree clean
```

---

**NOT:** `git help diff`, `git diff -h`, git help glossary`.
