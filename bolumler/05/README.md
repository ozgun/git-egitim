## 05: commit nedir?

* As a noun: A single point in the Git history; the entire history of a project is represented as a set of interrelated commits.
* As a verb: The action of storing a new snapshot of the project’s state in the Git history, by creating a new commit representing the current state of the index and advancing HEAD to point at the new commit.
* Bir klasörün içindeki dosyalarla birlikte belirli bir durumunu temsil eder.
* Her commit'in yazar, tarih, mesaj gibi bir meta datası vardır.
* Her commit'in bir veya birden fazla parent'ı vardır.
* Her commit, parent'ına bir referens bilgisini tutar.
* Her commit'in repo genelinde uniq bir tanımlayıcısı(identifier, hash) vardır.
* Commit identifier/hash SHA-1 algoritması kullanılarak oluşturulur.

---

**NOT:** `git help commit`, `git commit -h`, `git help glossary`.
