## 33: Son Sözler...

* Bir an önce git kullanmaya başlayalım(kullanmayanlar için).
* Mümkün olduğunca kısa commitler yapalım.
* Branchleri sevelim, PR'ları bağrımıza basalım.
* Commit mesajımız açıklayıcı olsun, ne yaptığımızdan ziyade neden yaptığımızı belirtelim.
* Sadece kodlarımızı değil, config dosyalarımızı da git ile takip edebiliriz.
* Tek başınıza geliştirme yapıyor olsanız bile versiyon kontrol sistemi kullanmanızı öneririm.
* Niçin bir versiyon takip sistemi kullanmak isteriz?
