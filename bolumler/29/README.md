## 29: PR(Pull Request) && Merge

Aşağıdaki adımları takip ederek `blog` branchi için bir PR(Pull Request)
oluşturup sorna Bitbucket'taki `blog` branchini Bitbucket'taki `master`
branchine merge edeceğiz.

* Bitbucket'ta `trakya-dev` repomuzu seçelim.
* Soldaki menüden "Pull Requests" linkini tıklayalım.
* Açılan sayfada sağ üst köşedeki "Create pull request" linkini tıklayalım.
* Sol kutuda `blog`, sağ kutuda `master` branchini seçili olduğuna emin olalım.
* Değişiklik yapmadan "Create pull request" butonuna tıklayalım.
* PR oluşturulduktan sonra gelen ekranda "Merge" butonuna tıklayalım.
* Soldaki menüden "Commits" linkine tıklayıp commit'i görelim.
* Tekrar soldaki menüden "Source" linkine tıklayıp, `blog.html` dosyasını görelim.
