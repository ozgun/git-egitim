## 25: Commitler Arasındaki Değişiklikleri Görmek

```
$ git log --oneline

$ git diff <commit1-hash>..<commit2-hash>
$ git diff <commit1-hash>..<commit2-hash> README.md

$ git log -p <commit1-hash>..<commit2-hash>
$ git log -p <commit1-hash>..<commit2-hash> README.md

$ git diff <commit1-hash>..HEAD index.html
$ git log -p <commit1-hash>..HEAD index.html
```

---

**NOT:** `git help diff`, `git help log`.
