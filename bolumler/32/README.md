## 32: git GUI

* Linux + Windows + Linux: https://git-scm.com/download/guis/
* Windows + Mac OS: https://www.sourcetreeapp.com
* Windows: https://desktop.github.com
* Linux + Windows + Linux: https://www.gitkraken.com (Ücretli ama ücretsiz)

Aşağıdaki komut ile git-scm.com'dan indirilen GUI çalıştırılır:

```
$ git gui
```
