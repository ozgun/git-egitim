## 22: bootstrap Branch'hini master'e Merge Edelim

Bu adımda `bootstrap` branch'ini `master` branchine merge edeceğiz, yani
`bootstrap` branch'indeki commit'leri `master`a aktaracağız.

```
$ git checkout master  #  "master"a geçelim
```

Proje klasöründeki dosyaları kontrol edelim:

* `index.html`deki değişiklik ne durumda?
* `css/index.css` duruyor mu?

Komutlar:

```
$ git status          # On branch master

$ git merge bootstrap # "bootstrap" branch'ini "master"a merge eder
$ git status          # On branch master
                      # nothing to commit, working tree clean
$ git log --oneline   # commit loglarına bakalım
```

Proje klasöründeki dosyaları tekrar kontrol edelim:

* `index.html`deki css bootstrap olmuş mu?
* `css/index.css` silinmiş mi?

---

**NOT:** `git help glossary` -> "fast-forward" ve "merge commit"'i arat.
