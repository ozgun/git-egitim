## 19: Son Commit'i Geri Almak(revert)

```
$ git log -1 --oneline  # Son commit'in hash'ini bulalım
$ git revert --no-edit <commit-hash> # Son commit'i geri alalım.
$ git status            # nothing to commit, working tree clean
```

---

**NOT 1:** `--no-edit` parametresini harici bir editor açmamak için kullandık.
**NOT 2:** `git help revert`
