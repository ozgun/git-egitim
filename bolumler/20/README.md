## 20: Branching(Dallanma)

* Branching: Ana hattan ayrılıp, paralel bir hatta çalışma işlemi.
* Bir repo oluşturduğumuzda otomatik olarak bir branch(master) oluşur.
* Yeni bir branch oluşturduğumuzda bir pointer/etiket oluşturulur.
* Branching dediğimiz şey aslında bu pointer'in commitler arasında gezinmesidir.
* Branchleri oluşturmak ve branchler arası geçişler kolay ve hızlıdır.
* Takımlar için branchler olmazsa olmazdır.
* Code review kolaylıkla yapılabilir.

Adı `bootstrap` olan bir branch oluşturalım:

```
$ git branch  # Mevcut branchleri(local) listeleyelim: master

$ git branch bootstrap  # Yeni bir branch oluşturur.
$ git branch  # Hala master'dayız. "*" işareti hangi branch'te olduğumuzu belirler.

$ git checkout bootstrap # bootstrap'e geçtik
$ git branch #  "*" işareti bootstrap'in yanında mı?

$ git checkout master # master'e geçelelim
$ git status # "On branch master"?

$ git branch -d bootstrap #  bootstrap branch'ini silelim
$ git branch  # Silinmiş mi?
```


Bu sefer kısa yol ile `bootstrap` branch'ini oluşturalım ve o branch'e geçis yapalım:

```
$ git checkout -b bootstrap # bootstrap branch'ini oluşturur ve o branch'e geçilir.
$ git branch                # "*" nerde?
```

---

**NOT:** `git help branch`, `git help checkout`, [Pro Git e-Kitap: Git-Branching-Branches-in-a-Nutshell](https://git-scm.com/book/tr/v2/Git-Branching-Branches-in-a-Nutshell)
