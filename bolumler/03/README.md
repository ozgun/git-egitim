## 03: Yeni Bir Repository Oluşturulması

Senaryo: Trakya'daki tüm yazılım geliştiricileri ve grupları bir çatı altında
toplayan basit bir web sitesi yapılması.

```
$ mkdir trakya-dev  # Proje klasörünün oluşturulması
$ cd trakya-dev/    # Oluşturduğumuz proje klasörüne geçiş yapalım
$ ls -a             # Klasörün boş olduğuna emin olalım
$ git init          # Initialized empty Git repository in /.../trakya-dev/.git
$ ls -a             # .git klasörünün yaratılmış olması gerekir
$ ls -l .git/       # .git klasöründeki dosyaları listele
```

* `git init` komutu bir klasörü git repository'sine dönüştürür.
* git reposu `.git` klasöründe tutulur.
* Repository ile ilgili herşey(objects, metadata, vs) `.git` klasöründe tutulur.
* Git repository'sine bir çeşit arşiv veya veritabanı de diyebiliriz.
* Repository yerine kısaca "repo" deriz, der miyiz :)

---

**NOT 1**: Bu adımdan sonraki tüm komutlar `trakya-dev` klasöründe çalıştırılacaktır.

**NOT 2:** `git help init`, `git init -h`, [Pro Git e-Kitap: Getting a Git Repository](https://git-scm.com/book/tr/v2/Git-Temelleri-Getting-a-Git-Repository)
