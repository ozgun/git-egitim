## 06: add file && commit

* `index.html` dosyasını oluşturalım.
* Indentation için tab yerine space/boşluk kullanalım.

```
<!DOCTYPE html>
<html>
  <body>
    <h1>Trakya Yazılım Geliştiricileri Platformu</h1>
    <div>2018</div>
  </body>
</html>
```

`index.html` dosyasını repository'ye ekleyelim:

```
$ git status         # index.html -> Untrackted files

$ git add index.html # index.html Staging Area'ya alınır(sahneleme)
$ git status         # Changes to be committed: index.html

$ git commit -m "index.html dosyası eklendi"
$ git status         # nothing to commit, working tree clean
```

* Working Tree    -> `add` -> Staging Area   -> `commit` -> Repository
* Çalışma Klasörü -> `add` -> Hazırlık Alanı -> `commit` -> Repository
* Working Tree/Directory: Dosyalarımızın bulunduğu çalışma alanı/klasörü
* Staging Area: Commit edilmeye hazır değişiklikler/içerik (Hazırlık Alanı)
* Repository: Commitlerin bulunduğu veritabanı/arşiv/etc (Git directory)

---

**NOT:** `git help status`, `git status -h`, `git help add`, `git add -h`, [Git’in-Temelleri](https://git-scm.com/book/tr/v2/Başlangıç-Git’in-Temelleri)
