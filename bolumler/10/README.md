## 10: git add -p && commit

Önce `index.html`ye "Yapım Aşamasında" yazısı ekleyeyim:

```
<!DOCTYPE html>
<html>
  <head>
    <link rel="stylesheet" href="css/main.css" type="text/css">
  </head>
  <body>
    <h1>Trakya Yazılım Geliştiricileri Platformu</h1>
    <p>Yapım Aşamasında</p>
  </body>
</html>
```

Sonra `main.css`e "color" ekleyelim:

```
body {
  background-color: #ccc;
  color: #000;
}
```

Komutlar:

```
$ git status  # Changes not staged for commit: index.html ve css/main.css

$ git add -p  # Değişiklikleri parça(hunk) parça staging area'ya ekleyelim
$ git status  # Changes to be committed: index.html ve css/main.css

$ git commit -m "Font rengi değişti. Yapım Aşamasında eklendi."
$ git status  # nothing to commit, working tree clean
```

---

**NOT:** `git add -p` yeni oluşturulan(yani track edilmeyen) dosyaları eklemez!
