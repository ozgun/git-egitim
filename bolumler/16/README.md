## 16: Yaptığımız Değişikliklerden Vazgeçmek (reset & checkout)

Önce `css/index.css` dosyasını açıp, aşağıdaki gibi güncelleyelim.

```
body {
  background-color: #ccc;
}
```

Sonra da bu değişikliği staging area'ya alalım:

```
$ git status  # Changes not staged for commit:
              # modified:   css/index.css

$ git add css/index.css
$ git status  # Changes to be committed:
              # modified:   css/index.css
```

Son değişiklikleri staging area'dan çıkaralım:

```
$ git reset HEAD css/index.css # Unstaged changes after reset:
                               # M css/index.css
$ git status  # Changes not staged for commit:
              # modified:   css/index.css
```

NOT: `git reset HEAD` parametre almadan çalıştırılırşa staging area'daki tüm
değişiklikler staging area'dan çıkarılır.

Sonra da yaptığımız değişikliği de iptal edelim, yani index.css dosyasını eski haline getirelim:

```
$ git checkout -- css/index.css  # Değişikliği geri alalım.
$ cat css/index.css              # İçerik eski haline gelmiş mi?
$ git status                     # nothing to commit, working tree clean
```

---

**NOT 1**: `checkout`un diğer bir kullanımı working tree'de yaptığımız
değişiklikleri geri almak(restore). Yani, henüz staging area'ya göndermediğimiz
dosyalardaki değişiklikleri geri almak.

**NOT 2:** `git help reset`, `git help checkout`, [Pro Git e-Kitap: Undoing Things](https://git-scm.com/book/tr/v2/Git-Temelleri-Undoing-Things)
