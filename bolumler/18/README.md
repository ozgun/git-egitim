## 18: Herşeyin eklenmesi: git add --all


Aşağıdaki içeriği kullanarak, `iletisim.html` dosyasını oluşturalım:

```
<!DOCTYPE html>
<html>
  <head>
    <link rel="stylesheet" href="css/main.css" type="text/css">
    <link rel="stylesheet" href="css/iletisim.css" type="text/css">
  </head>
  <body>
    <h1>İletişim</h1>
    <p>Yapım Aşamasında</p>
    <footer>Powered by Startup Trakya</footer>
  </body>
</html>
```

Sonra aşağıdaki gibi bir `css/iletisim.css` dosyası oluşturalım:

```
h1 {
  color: green;
}
```

Ardından, `css/index.css` dosyasının adını `css/main.css` olarak değiştirelim.

```
$ mv css/index.css css/main.css
```

Daha sonra `index.html` dosyasına bir link ekleyelim ve css ile ilgili satırı güncelleyelim:

```
<!DOCTYPE html>
<html>
  <head>
    <link rel="stylesheet" href="css/main.css" type="text/css">
  </head>
  <body>
    <h1>Trakya Yazılım Geliştiricileri Platformu</h1>
    <nav>
      </ul>
        </li><a href="iletisim.html">İletişim</a></li>
      </ul>
    </nav>
    <p>Yapım Aşamasında</p>
    <footer>Powered by Startup Trakya</footer>
  </body>
</html>
```

Son olarak da "yanlışlıkla" README.md dosyasını silelim:

```
$ rm README.md
```

Komutlar:

```
$ git status  # Changes not staged for commit: 
              #   modified:   css/index.css
              #   modified:   index.html
              # Untracked files:
              #   css/iletisim.css
              #   iletisim.html

$ git add --all # Değişiklikleri staging area'ya atalım

$ git status  # Changes to be committed:
              #   deleted:    README.md
              #   new file:   css/iletisim.css
              #   modified:   css/index.css
              #   new file:   iletisim.html
              #   modified:   index.html

$ git commit -m "İletişim safası eklendi"
$ git status  # nothing to commit, working tree clean
```
