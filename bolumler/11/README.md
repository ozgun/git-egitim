## 11: diff --staged

Ana sayfaya yani `index.html` sayfasına bir footer ekleyelim:

```
<!DOCTYPE html>
<html>
  <head>
    <link rel="stylesheet" href="css/main.css" type="text/css">
  </head>
  <body>
    <h1>Trakya Yazılım Geliştiricileri Platformu</h1>
    <p>Yapım Aşamasında</p>
    <footer>Powered by Startup Trakya</footer>
  </body>
</html>
```

Komutlar:

```
$ git status  # Changes not staged for commit: index.html
$ git diff    # Yaptığımız değişiklikleri görelim

$ git add index.html # staging area'ya ekleyelim
$ git status         # Changes to be committed: index.html
$ git diff           # WTF?

$ git diff --staged  # staging area'daki değişiklikleri gösterir.

$ git commit -m "Footer eklendi"
$ git status  # nothing to commit, working tree clean
```

---

**NOT:** `git help diff`
