## 21: Branch'te commit yapalım

```
$ git status  # "On branch bootstrap" ?
```

Şimdi, `index.html` dosyasındaki css satırını değiştirelim:

```
<!DOCTYPE html>
<html>
  <head>
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
  </head>
  <body>
    <h1>Trakya Yazılım Geliştiricileri Platformu</h1>
    <p>Yapım Aşamasında</p>
    <footer>Powered by Startup Trakya</footer>
  </body>
</html>
```

Komutlar:

```
$ git status  # On branch bootstrap
              # Changes not staged for commit:
              # modified:   index.html

$ git commit -a -m "Bootstrap kullanmaya başladık"
```

Sonra `css/index.css` dosyasını silelim ve commit edelim:

```
$ git rm css/index.css # Önce diskten siler,
                       # Sonra da staging area'ya değişikliği gönderir. 

$ git status  # On branch bootstrap
              # Changes to be committed:
              # deleted:    css/index.css 

$ git commit -m "index.css silindi"
$ git status  # nothing to commit, working tree clean
```
