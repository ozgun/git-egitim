## 24: Commit Loglarında Arama

```
# İçinde README geçen commit mesajlarında arama yapar:
$ git log --grep="README"

# Yukarıdakine ek olarak bir de diff'leri göster
$ git log --grep="README" -p

# index.html'yi etkileyen commitleri listeler:
$ git log -- index.html
$ git show <commit-hash>

# Yukarıdakinin kısa yolu:
$ git log -p --index.html

# Dosya içeriklerinde arama yapar:
$ git log -S"bootstrap"

# Yukarıdakine ek olarak bir de diff'leri göster
$ git log -S"bootstrap" -p

# Yukarıdakine ek olarak  tüm branchlerde arama yapılır
$ git log -S"bootstrap" -p --all

# Eski bir committe yapılan değişiklikleri görmek:
$ git log -p -1 <commit-hash>

# Yukarıdakinin kısa yolu:
$ git show <commit-hash>

# Son committe yapılan değişiklikleri görmek:
$ git show HEAD
$ git show HEAD~0

# Sondan bir önceki(parent) committe yapılan değişiklikleri görmek
$ git show HEAD^
$ git show HEAD~1

# Sondan bir öncekinden önceki :) - 2nd ancestor
$ git show HEAD^^
$ git show HEAD~2

# Sondan 5 önceki...
$ git show HEAD~5
```

---

**NOT:** `git help log`, `git help show`, `git help revisions`.
