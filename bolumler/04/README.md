## 04: config && commit

Proje klasörümüzde içeriği aşağıdaki gibi olan bir `README.md` dosyası oluşturalım.

```
## Trakya Yazılım Geliştiricileri Platformu

Trakya'daki tüm yazılım geliştiricileri ve grupları bir çatı altında toplamayı amaçlayan bir web sitesi.
```

Komutlar:

```
$ ls -a  # README.md ve .git klasörü dışındaki tüm dosyaları silelim.
$ git add README.md
$ git commit -m "README.md eklendi" # Bu komuttan sonra konfigurasyon gerekebilir.
$ git status                        # nothing to commit, working tree clean
```
Eğer yukarıdaki komuttan sonra konfigurasyon eksikliği ile ilgili şöyle bir hata
alınırsa:

```
*** Please tell me who you are.

Run

git config --global user.email "you@example.com"
git config --global user.name "Your Name"

to set your account's default identity.
Omit --global to set the identity only in this repository.

fatal: unable to auto-detect email address (got 'root@12ed0e79ddb9.(none)')
```

Kendimizi "git"e tanıtalım. Bitbucket'ta kullandığımız veya kullanacağımız
eposta adresini girelim:

```
$ git config --global user.email "eposta@example.com"
$ git config --global user.name "Adınız Soyadınız"
$ git config --list --global
```

Konfigurasyon tamamlandıktan sonra ilk commit'imiz yapalım:

```
$ git commit -m "README.md dosyası oluşturuldu"
$ git status # nothing to commit, working tree clean
```

Detaylar az sonra :)

---

**NOT**: `git help config`
