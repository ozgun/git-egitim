## 30: Remote'taki master'i Pull Etmek

Bir önceki adımda Bitbucket'taki master'a Bitbucket'taki `blog` branchini PR
request aracılığıyla merge etmiştik, şimdi remote'taki master branch'i
localimize çekelim(pull)

```
$ git checkout master
$ git status
$ git branch --all

$ git pull # remote'taki master'i local'deki master branch'e çek (fetch & merge)

$ git branch --all # blog
                   # bootstrap
                   # * master
                   # remotes/origin/blog
                   # remotes/origin/master

$ git log --all --graph --oneline
```

---

**NOT:** `git help pull`

