## 14: Sildiğimiz dosyayı commit etmeden geri getirmek

`index.html` dosyasını dosya yöneticisi veya `rm index.html` komutu ile silelim. Sonra da:

```
$ git status  # Changes not staged for commit -> deleted: index.html

$ git checkout -- index.html # Sildiğimiz dosyayı geri getirelim.
$ git status  # nothing to commit, working tree clean
```

---

**NOT 1:** `checkout`un bir kullanımı working tree'den silinen dosyaların geri getirilmesi(restore).

**NOT 2:** `git help checkout`


