## 07: add directory && commit

1. `css` klasörünü oluşturalım: `mkdir css`
2. `css` klasöründe `main.css` dosyasını oluşturalım.
3. `main.css` dosyasının içeriğini asağıdaki gibi yapalım:

```
body {
  background-color: #ccc;
}
```

Komutlar:

```
$ git status   # css/ -> Untracked files

$ git add css  # css klasorünü dosyalarıyla birlikte staging area'ya atalım
$ git status   # Changes to be committed...

$ git commit -m "css klasörü eklendi" # Değişikikleri repo'ya kaydedelim.
$ git status   # nothing to commit, working tree clean
```

---

**NOT:** `git help add`, `git add -h`.
