## 26: Bitbucket'ta Repo Oluşturulması ve Push

* Bitbucket'a bir hesap açalım ya da mevcut hesabımıza giriş yapalım.
* Erişim seviyesi "private" olan `trakya-dev` adında bir repo oluşturalım.
* Repo için otomatik README.md dosyası oluşturulMASIN! (Include a README? : No)

Önce remote repo adresini ekleyelim:

```
$ git status # On branch master, nothing to commit...

# Aşağıdaki "username"leri kendi kullanıcı adınız ile değiştirin.
# "https" ile başlayan repo url'si olduğuna emin olun!
$ git remote add origin https://username@bitbucket.org/username/trakya-dev.git

$ git remote -v  # remote reponun eklendiğini kontrol edelim
```

Sonra local repomuzu remote'a push edelim: 

```
$ git status  # On branch master, nothing to commit...

$ git push -u origin master  # Local repo'yu remote'a push eder
                             # * [new branch]  master -> master
                             # Branch master set up to track remote branch master from origin.

$ git branch --all # Hem local hem de track ettiğimiz remote branchleri listeleyim
                   # bootstrap                         
                   # * master                            
                   # remotes/origin/master  
```

Bitbucket'a girip, oluşturduğumuz repoyu biraz inceleyelim.

---

**NOT 1:** Public repolara herkese açık olduğu için, hassas bilgileri public
repolarda tutmayınız(bakınız .gitignore)

**NOT 2:** `git help remote`, `git help push`, `git help glossary`, [Pro Git e-Kitap: Working with Remotes](https://git-scm.com/book/tr/v2/Git-Temelleri-Working-with-Remotes)
