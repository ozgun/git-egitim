## 31: Repo Klonlamak

`git clone` ile uzaktaki bir sunucudaki(Github, Bitbucket, Gitlab, vs) "remote
repository"yi kendi bilgisayarımıza kopyalarız/indiririz. Kendi bilgisayarımızdaki
repo'ya "local repository" diyebiliriz.

```
$ cd /tmp  # Proje klasörü dışında bir klasöre geçelim. 
$ ls       # "deneme" diye bir klasör olmadığına emin olalım

$ git clone https://ozguntest@bitbucket.org/ozguntest/deneme.git

$ cd deneme
$ git log
```

---

**NOT:** `git help clone`, `git help glossary` -> "remote repository"
