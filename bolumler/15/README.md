## 15: Rename

Önce, `css` klasöründeki `main.css` dosyasının adını `index.css` olarak değiştirelim.

```
$ mv css/main.css css/index.css

$ git status  # Changes not staged for commit -> deleted:    css/main.css
              # Untracked files -> css/index.css
```

Sonra da commit edelim:

```
$ git add css/index.css
$ git rm css/main.css
$ git status  # Changes to be committed:
              # renamed:    css/main.css -> css/index.css

$ git commit -m "main.css dosyası index.css olarak değiştirildi"
$ git status  # nothing to commit, working tree clean
```

---

**NOT:** `git help rm`
