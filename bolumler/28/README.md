## 28: Yeni Branch'i Push Etmek

Önce `blog` adında yeni bir branch oluşturup `blog` branch'ine geçis yapalım.

```
$ git checkout -b blog
$ git status  # On branch blog, nothing to commit...
```

Sonra `blog.html` dosyası oluşturulalım:

```
<!DOCTYPE html>
<html>
</html>
```

Sonra değişiklikleri commit edelim:

```
$ git add blog.html
$ git commit -m 'Blog eklendi'

$ git status  # On branch blog, nothing to commit...
$ git branch --all
```

Sonra da localdeki blog branchini remote'a push edelim:

```
$ git push origin blog  # Create pull request for blog:
                        # https://bitbucket.org/username/trakya-dev/pull-requests/new?source=blog&t=1
```

Bitbucket'a girip `blog` branchini oluşup oluşmadığını kontol edelim.

---

**NOT:** `git help glossary` -> "origin" ?
