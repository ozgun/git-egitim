## 17: Değiştirilen Tüm Dosyaların Commit Edilmesi

Önce, `index.html` dosyasını aşağıdaki gibi güncelleyelim:

```
<!DOCTYPE html>
<html>
  <head>
    <link rel="stylesheet" href="css/index.css" type="text/css">
  </head>
  <body>
    <h1>Trakya Yazılım Geliştiricileri Platformu</h1>
    <p>Yapım Aşamasında</p>
    <footer>Powered by Startup Trakya</footer>
  </body>
</html>
```

Sonra, `css/index.css` dosyasını aşağıdaki gibi güncelleyelim:

```
body {
  background-color: #ccc;
  color: black;
}
```

Komutlar:

```
$ git status  # Changes not staged for commit:
              # modified:   css/index.css
              # modified:   index.html

$ git commit -a -m "index.html ve index.css güncellendi"
$ git status  # nothing to commit, working tree clean
```

---

**NOT:** `-a` parametresi sadece hali hazırda git reposunda olan ve değişiklik
yapılan dosyaları ekler. Yeni dosyaları eklemez!
