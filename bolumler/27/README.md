## 27: Değişikliklerin Remote Repo'ya Push Edilmesi

Önce `index.html` sayfamıza bootstrap için gerekli meta taglerini ekleyelim:

```
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
  </head>
  <body>
    <h1>Trakya Yazılım Geliştiricileri Platformu</h1>
    <p>Yapım Aşamasında</p>
    <footer>Powered by Startup Trakya</footer>
  </body>
</html>
```

Komutlar:

```
$ git commit -a -m "Bootstrap meta tagleri eklendi"
$ git status  # On branch master
              # Your branch is ahead of 'origin/master' by 1 commit.

$ git log --oneline --graph --all
$ git push    # local master'i remote'taki master'a push edelim.
```
