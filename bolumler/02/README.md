## 02: Git Nedir?

* "The stupid content tracker" --- Git Manual
* "Hızlı" ve "dağıtık" bir versiyon/sürüm/revizyon kontrol sistemi
* Bir klasördeki dosyalarda yaptığımız değişiklikleri tuttuğumuz bir ...???
* Yaptığımız değişiklikler(history) arasında gezinebileceğimiz bir yapısı var
* Sadece kendi bilgisayarımızda bir sunucu/servis kurmadan da çalışabiliriz
* Yaptığımız değişiklikleri/dosyaları diğer geliştiricilerle paylaşabiliriz
* İster komut satırından ister GUI(kullanıcı arayüzü) aracılığıyla kullanabiliriz
* Remote repolar için Github/Bitbucket gibi servisleri veya kendi sunucumuzu kullanabiliriz
* Linux Torvalds tarafından geliştirilmiştir

---

**NOT:** [Pro Git e-Book](https://git-scm.com/book/en/v2), [Pro Git e-Book Türkçe Çevirisi](https://git-scm.com/book/tr/v2), `git help git`, `git help tutorial`, `git help tutorial-2`
