## 23: Merge Commit(Fast-forward Olmayan)

Önce `README.md` dosyasını `master` branchteyken aşağıdaki gibi güncelleyelim.

```
## Trakya Yazılım Geliştiricileri Platformu

Trakya'daki tüm yazılım geliştiricileri ve grupları bir çatı altında toplamayı amaçlayan bir web sitesi.

İletişim: info@example.com
```

Sonra commit edelim:

```
$ git status  # on branch master, ...working tree clean

$ git commit -a -m "README'ye iletişim bilgisi eklendi"
$ git status  # nothing to commit, working tree clean
```

Sonra `bootstrap` branch'ine geçelim:

```
$ git checkout bootstrap
$ git status  # On branch bootstrap
```

Sonra, `index.html`ye Bootstrap JS kodu ekleyelim.

```
<!DOCTYPE html>
<html>
  <head>
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
  </head>
  <body>
    <h1>Trakya Yazılım Geliştiricileri Platformu</h1>
    <p>Yapım Aşamasında</p>
    <footer>Powered by Startup Trakya</footer>
  </body>
</html>
```

Tekrar commit edelim:

```
$ git commit -a -m "Bootstrap JS kodu eklendi."
$ git status # On branch bootstrap, nothing to commit...
```

Ve `master` branch'e merge edelim:

```
$ git checkout master
$ git status  # On branch master,  nothing to commit...

$ git merge --no-edit bootstrap # "bootstrap"i "master"a merge edelim
                                # Merge made by the 'recursive' strategy.

$ git status # On branch master, nothing to commit
$ git log --oneline
$ git log --oneline --graph # Bir de şekilli loga bakalım
```

Şekilli log'un şöyle görünmesi lazım:

```
*   5a3021a (HEAD -> master) Merge branch 'bootstrap'                    
*   |\                                  
*   | * 5e356ec (bootstrap) Bootstrap JS kodu eklendi.                       
*   * | 5ceaae5 README'ye iletişim bilgisi eklendi                           
*   |/                                  
*   * db32cb5 index.css silindi         
*   * da923cd Bootstrap kullanmaya başladık                                  
*   * 43b1f3a Revert "İletişim safası eklendi" 
```
