## Uygulamalı "Git" Başlangıç Eğitimi

Bu eğitim başlangıç seviyesinde bir eğitimidir. "Git" deneyimi olmayan veya
çok az bilgisi olanların bu eğitimi tamamladıktan sonra kendi projelerinde
"git" kullanacak seviyede olmaları hedeflenmektedir.

Uygulamalı bir eğitim olacağı için katılımcıların kendi bilgisayarlarını
yanlarında getirmeleri gerekmektedir.

Eğitimin süresi yaklaşık 3 saat olarak düşünülmüştür, eğitimin gidişatına göre
bu süre uzayabilir veya kısalabilir.

Bir adımdaki tüm komutlar herkes tarafından çalıştırılmadan sonraki adıma geçiş
yapılmayacaktır.

Internet bağlatısı gerekmektedir, lütfen internet bağlantınız olup olmadığını
kontrol ediniz.

### Konu Başlıkları

* Git kurulumu(Windows, Linux, Mac OS)
* Git reposu oluşturma(init)
* Temel İşlemler(diff, status, add, rm, mv, commit, config)
* .gitignore
* Geçmiş commit'leri inceleme(log, grep)
* Branch işlemleri(checkout, merge)
* Bitbucket: Hesap açma, ayarlar, repo oluşturma
* Bitbucket: Remote repo işlemleri(push, pull, clone)
* GUI araçlarına giriş
